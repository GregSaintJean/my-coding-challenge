package com.solution;

import java.util.*;

public class Program {
	
	private static int MINUTES_IN_AN_HOUR = 60;
	
	public static List<StringMeeting> calendarMatching(
		      List<StringMeeting> calendar1,
		      StringMeeting dailyBounds1,
		      List<StringMeeting> calendar2,
		      StringMeeting dailyBounds2,
		      int meetingDuration) {
		
		if(dailyBounds1 == null 
				|| dailyBounds2 == null) {
			return null;
		}
		
		Meeting dailyBound1 = parseStringMeeting(dailyBounds1);
		Meeting dailyBound2 = parseStringMeeting(dailyBounds2);
		
		if((calendar1 == null 
				&& calendar2 == null) || (calendar1.size() == 0 && calendar2.size() == 0)) {
			
			List<StringMeeting> ans = new ArrayList<>();
			
			if(dailyBound1.startingTimeCompareTo(dailyBound2) > 0) {
				
				if(dailyBound1.endingTimeCompareTo(dailyBound2) > 0) {
					
					Meeting meeting 
					= new Meeting(
							dailyBound1.startingHour,
							dailyBound1.startingMinute,
							dailyBound2.endHours,
							dailyBound2.endMinutes);
					
					ans.add(convertMeetingtoStringMeeting(meeting));
					return ans;
					
				} else if(dailyBound1.endingTimeCompareTo(dailyBound2) < 0) {
					
					Meeting meeting 
					= new Meeting(
							dailyBound1.startingHour,
							dailyBound1.startingMinute,
							dailyBound1.endHours,
							dailyBound1.endMinutes);
					
					ans.add(convertMeetingtoStringMeeting(meeting));
					return ans;
					
				} else {
					
					Meeting meeting 
					= new Meeting(
							dailyBound1.startingHour,
							dailyBound1.startingMinute,
							dailyBound1.endHours,
							dailyBound1.endMinutes);
					
					ans.add(convertMeetingtoStringMeeting(meeting));
					return ans;
					
				}
				
			} else if(dailyBound1.startingTimeCompareTo(dailyBound2) < 0) {
				
				if(dailyBound1.endingTimeCompareTo(dailyBound2) > 0) {
					
					Meeting meeting 
					= new Meeting(
							dailyBound2.startingHour,
							dailyBound2.startingMinute,
							dailyBound2.endHours,
							dailyBound2.endMinutes);
					
					ans.add(convertMeetingtoStringMeeting(meeting));
					return ans;
					
				} else if(dailyBound1.endingTimeCompareTo(dailyBound2) < 0) {
					
					Meeting meeting 
					= new Meeting(
							dailyBound2.startingHour,
							dailyBound2.startingMinute,
							dailyBound1.endHours,
							dailyBound1.endMinutes);
					
					ans.add(convertMeetingtoStringMeeting(meeting));
					return ans;
					
				} else {
					
					Meeting meeting 
					= new Meeting(
							dailyBound2.startingHour,
							dailyBound2.startingMinute,
							dailyBound2.endHours,
							dailyBound2.endMinutes);
					
					ans.add(convertMeetingtoStringMeeting(meeting));
					return ans;
					
				}
				
			} else {
				
				if(dailyBound1.endingTimeCompareTo(dailyBound2) > 0) {
					Meeting meeting 
					= new Meeting(
							dailyBound1.startingHour,
							dailyBound1.startingMinute,
							dailyBound2.endHours,
							dailyBound2.endMinutes);
					
					ans.add(convertMeetingtoStringMeeting(meeting));
					return ans;
				} else if(dailyBound1.endingTimeCompareTo(dailyBound2) < 0) {
					Meeting meeting 
					= new Meeting(
							dailyBound1.startingHour,
							dailyBound1.startingMinute,
							dailyBound1.endHours,
							dailyBound1.endMinutes);
					
					ans.add(convertMeetingtoStringMeeting(meeting));
					return ans;
				} else {
					Meeting meeting 
					= new Meeting(
							dailyBound1.startingHour,
							dailyBound1.startingMinute,
							dailyBound1.endHours,
							dailyBound1.endMinutes);
					
					ans.add(convertMeetingtoStringMeeting(meeting));
					return ans;
				}
				
			}
		}
		return findAvailability(
				parseStringMeetingCalendars(calendar1),
				dailyBound1,
				parseStringMeetingCalendars(calendar2),
				dailyBound2,
				meetingDuration);
	}
	
	private static List<StringMeeting> findAvailability(
			List<Meeting> meetingCalendar1,
			Meeting dailyBound1,
			List<Meeting> meetingCalendar2,
			Meeting dailyBound2,
			int meetingDuration) {
		
		List<Meeting> ans = new ArrayList<>();
		Meeting globalDailyBounds = mergeDailyBounds(dailyBound1, dailyBound2);
		List<Meeting> mergedCalendarSchedules = mergeCalendarSchedules(meetingCalendar1, meetingCalendar2, globalDailyBounds);
		
		Collections.sort(mergedCalendarSchedules);
		
		for(int i = 0; i < mergedCalendarSchedules.size(); i++) {
			
			if(i == 0) {
				
				Meeting firstMeeting = mergedCalendarSchedules.get(i);
				
				if(isMeetingWithinBounds(globalDailyBounds, firstMeeting)) {
					
					if(globalDailyBounds.startingHour < firstMeeting.startingHour) {
						
						Time currentTime = new Time(globalDailyBounds.startingHour, globalDailyBounds.startingMinute);
						Time potentialEndTime = calculatePotentialEndTime(currentTime, meetingDuration);
						
						if(!firstMeeting.isTimeIn(potentialEndTime) && !firstMeeting.isTimeStart(currentTime)) {
							ans.add(new Meeting(globalDailyBounds.startingHour, globalDailyBounds.startingMinute, firstMeeting.startingHour, firstMeeting.startingMinute));
						}
						
					} else if(globalDailyBounds.startingHour == firstMeeting.startingHour 
							&& globalDailyBounds.startingMinute < firstMeeting.startingMinute) {
						
						Time currentTime = new Time(globalDailyBounds.startingHour, globalDailyBounds.startingMinute);
						Time potentialEndTime = calculatePotentialEndTime(currentTime, meetingDuration);
						
						if(!firstMeeting.isTimeIn(potentialEndTime) && !firstMeeting.isTimeStart(currentTime)) {
							ans.add(new Meeting(globalDailyBounds.startingHour, globalDailyBounds.startingMinute, firstMeeting.startingHour, firstMeeting.startingMinute));
						}
					}
					
					if(mergedCalendarSchedules.size() > 1) {
						int j = i + 1;
						
						Meeting meetingOne = mergedCalendarSchedules.get(i);
						Meeting meetingTwo = mergedCalendarSchedules.get(j);
						
						if(meetingOne.endingTimeCompareTo(meetingTwo) > 0) {
							
							Time currentTime = new Time(meetingTwo.endHours, meetingTwo.endMinutes);
							Time potentialEndTime = calculatePotentialEndTime(currentTime, meetingDuration);
							
							if(!meetingOne.isTimeIn(potentialEndTime) && !meetingOne.isTimeStart(currentTime)) {
								ans.add(new Meeting(meetingTwo.endHours, meetingTwo.endMinutes, meetingOne.startingHour, meetingOne.startingMinute));
							}
							
						} else if(meetingTwo.endingTimeCompareTo(meetingOne) > 0) {
							
							Time currentTime = new Time(meetingOne.endHours, meetingOne.endMinutes);
							Time potentialEndTime = calculatePotentialEndTime(currentTime, meetingDuration);
							
							if(!meetingTwo.isTimeIn(potentialEndTime) && !meetingTwo.isTimeStart(currentTime)) {
								ans.add(new Meeting(meetingOne.endHours, meetingOne.endMinutes, meetingTwo.startingHour, meetingTwo.startingMinute));
							}
							
						} else {
							Time currentTime = new Time(meetingOne.endHours, meetingOne.endMinutes);
							Time potentialEndTime = calculatePotentialEndTime(currentTime, meetingDuration);
							
							if(!meetingTwo.isTimeIn(potentialEndTime) && !meetingTwo.isTimeStart(currentTime)) {
								ans.add(new Meeting(meetingOne.endHours, meetingOne.endMinutes, meetingTwo.startingHour, meetingTwo.startingMinute));
							}
						}
					} else {
						if(globalDailyBounds.endHours > firstMeeting.endHours) {
							
							Time currentTime = new Time(firstMeeting.endHours, firstMeeting.endMinutes);
							Time potentialEndTime = calculatePotentialEndTime(currentTime, meetingDuration);
							
							if(isEndTimeWithinBounds(globalDailyBounds, potentialEndTime)) {
								ans.add(new Meeting(firstMeeting.endHours, firstMeeting.endMinutes, globalDailyBounds.endHours, globalDailyBounds.endMinutes));
							}
						} else if(globalDailyBounds.endHours == firstMeeting.endHours && globalDailyBounds.endMinutes > firstMeeting.endMinutes) {
							
							Time currentTime = new Time(firstMeeting.endHours, firstMeeting.endMinutes);
							Time potentialEndTime = calculatePotentialEndTime(currentTime, meetingDuration);
							
							if(isEndTimeWithinBounds(globalDailyBounds, potentialEndTime)) {
								ans.add(new Meeting(firstMeeting.endHours, firstMeeting.endMinutes, globalDailyBounds.endHours, globalDailyBounds.endMinutes));
							}
						}
					}
				}
				
			} else if(i == mergedCalendarSchedules.size()-1) {
				Meeting lastMeeting = mergedCalendarSchedules.get(i);
				
				if(globalDailyBounds.endHours > lastMeeting.endHours) {
					
					Time currentTime = new Time(lastMeeting.endHours, lastMeeting.endMinutes);
					Time potentialEndTime = calculatePotentialEndTime(currentTime, meetingDuration);
					
					if(isEndTimeWithinBounds(globalDailyBounds, potentialEndTime)) {
						ans.add(new Meeting(lastMeeting.endHours, lastMeeting.endMinutes, globalDailyBounds.endHours, globalDailyBounds.endMinutes));
					}
				} else if(globalDailyBounds.endHours == lastMeeting.endHours && globalDailyBounds.endMinutes > lastMeeting.endMinutes) {
					
					Time currentTime = new Time(lastMeeting.endHours, lastMeeting.endMinutes);
					Time potentialEndTime = calculatePotentialEndTime(currentTime, meetingDuration);
					
					if(isEndTimeWithinBounds(globalDailyBounds, potentialEndTime)) {
						ans.add(new Meeting(lastMeeting.endHours, lastMeeting.endMinutes, globalDailyBounds.endHours, globalDailyBounds.endMinutes));
					}
				}
				
			} else {
				int j = i+1;
				
				Meeting meetingOne = mergedCalendarSchedules.get(i);
				Meeting meetingTwo = mergedCalendarSchedules.get(j);
				
				if(meetingOne.endingTimeCompareTo(meetingTwo) > 0) {
					
					Time currentTime = new Time(meetingTwo.endHours, meetingTwo.endMinutes);
					Time potentialEndTime = calculatePotentialEndTime(currentTime, meetingDuration);
					
					if(!meetingOne.isTimeIn(potentialEndTime) && !meetingOne.isTimeStart(currentTime)) {
						ans.add(new Meeting(meetingTwo.endHours, meetingTwo.endMinutes, meetingOne.startingHour, meetingOne.startingMinute));
					}
					
				} else if(meetingTwo.endingTimeCompareTo(meetingOne) > 0) {
					
					Time currentTime = new Time(meetingOne.endHours, meetingOne.endMinutes);
					Time potentialEndTime = calculatePotentialEndTime(currentTime, meetingDuration);
					
					if(!meetingTwo.isTimeIn(potentialEndTime) && !meetingTwo.isTimeStart(currentTime)) {
						ans.add(new Meeting(meetingOne.endHours, meetingOne.endMinutes, meetingTwo.startingHour, meetingTwo.startingMinute));
					}
				} else {
					
					Time currentTime = new Time(meetingOne.endHours, meetingOne.endMinutes);
					Time potentialEndTime = calculatePotentialEndTime(currentTime, meetingDuration);
					
					if(!meetingTwo.isTimeIn(potentialEndTime) && !meetingTwo.isTimeStart(currentTime)) {
						ans.add(new Meeting(meetingOne.endHours, meetingOne.endMinutes, meetingTwo.startingHour, meetingTwo.startingMinute));
					}
				}
			}
			
		}
		
		return convertMeetingsToStringMeetings(ans);
	}
	
	private static final List<Meeting> mergeCalendarSchedules(
			List<Meeting> meetingCalendar1,
			List<Meeting> meetingCalendar2,
			Meeting globalBounds){
		
		List<Meeting> resultCalendar = new ArrayList<>();
		Meeting meetingOne, meetingTwo, mergedMeeting;
		int i, j;
		i = j = 0;
		HashSet<String> done = new HashSet<>();
		
		meetingOne = meetingCalendar1.get(i);
		meetingTwo = meetingCalendar2.get(j);
		
		Time startTimeOne = new Time(meetingOne.startingHour, meetingOne.startingMinute);
		Time endTimeOne =  new Time(meetingOne.endHours, meetingOne.endMinutes);
		Time startTimeTwo = new Time(meetingTwo.startingHour, meetingTwo.startingMinute);
		Time endTimeTwo =  new Time(meetingTwo.endHours, meetingTwo.endMinutes);
		
		while(!(globalBounds.isTimeIn(startTimeOne) && globalBounds.isTimeIn(endTimeOne))) {
			resultCalendar.add(meetingOne);
			i++;
			meetingOne = meetingCalendar1.get(i);
			startTimeOne = new Time(meetingOne.startingHour, meetingOne.startingMinute);
			endTimeOne =  new Time(meetingOne.endHours, meetingOne.endMinutes);
		}
		
		while(!(globalBounds.isTimeIn(startTimeTwo) && globalBounds.isTimeIn(endTimeTwo))) {
			resultCalendar.add(meetingTwo);
			j++;
			meetingTwo = meetingCalendar2.get(j);
			startTimeTwo = new Time(meetingTwo.startingHour, meetingTwo.startingMinute);
			endTimeTwo =  new Time(meetingTwo.endHours, meetingTwo.endMinutes);
		}
		
		mergedMeeting = meetingCalendar1.get(i);
		
		while(i < meetingCalendar1.size() 
				|| j < meetingCalendar2.size()) {
			
			if(i >= meetingCalendar1.size()) {
				meetingOne = meetingCalendar1.get(i-1);
				meetingTwo = meetingCalendar2.get(j);
				
				if(mergedMeeting.overlapsMeeting(meetingTwo)) {
					mergedMeeting = mergeMeeting(mergedMeeting, meetingTwo);
					j++;
				} else {
					
					String check = mergedMeeting.startingHour + ":" + mergedMeeting.startingMinute + "|" + mergedMeeting.endHours + ":" + mergedMeeting.endMinutes;
					
					if(!done.contains(check)) {
						resultCalendar.add(mergedMeeting);
						done.add(check);
						if(meetingOne.overlapsMeeting(meetingTwo)) {
							mergedMeeting = mergeMeeting(meetingOne, meetingTwo);
							j++;
						} else {
							resultCalendar.add(meetingTwo);
							j++;
						}
					}
				}
				
			} else if(j >= meetingCalendar2.size()) {
				meetingOne = meetingCalendar1.get(i);
				meetingTwo = meetingCalendar2.get(j-1);
				
				if(mergedMeeting.overlapsMeeting(meetingOne)) {
					mergedMeeting = mergeMeeting(mergedMeeting, meetingOne);
					i++;
				} else {
					
					String check = mergedMeeting.startingHour + ":" + mergedMeeting.startingMinute + "|" + mergedMeeting.endHours + ":" + mergedMeeting.endMinutes;
					
					if(!done.contains(check)) {
						resultCalendar.add(mergedMeeting);
						done.add(check);
						if(meetingOne.overlapsMeeting(meetingTwo)) {
							mergedMeeting = mergeMeeting(meetingOne, meetingTwo);
							i++;
						} else {
							resultCalendar.add(meetingOne);
							i++;
						}
					}
				}
				
			} else {
				meetingOne = meetingCalendar1.get(i);
				meetingTwo = meetingCalendar2.get(j);
				
				if(mergedMeeting.overlapsMeeting(meetingOne)) {
					if(i != 0) {
						mergedMeeting = mergeMeeting(mergedMeeting, meetingOne);
					}
					i++;
				} else if(mergedMeeting.overlapsMeeting(meetingTwo)) {
					mergedMeeting = mergeMeeting(mergedMeeting, meetingTwo);
					j++;
				} else {
					
					if(meetingOne.overlapsMeeting(meetingTwo)) {
						
						String check = mergedMeeting.startingHour + ":" + mergedMeeting.startingMinute + "|" + mergedMeeting.endHours + ":" + mergedMeeting.endMinutes;
						
						if(!done.contains(check)) {
							resultCalendar.add(mergedMeeting);
							done.add(check);
						}
						
						mergedMeeting = mergeMeeting(meetingOne, meetingTwo);
						j++;
					} else {
						
						if(mergedMeeting.startingTimeCompareTo(meetingTwo) > 0) {
							resultCalendar.add(meetingTwo);
							j++;
						} else if(mergedMeeting.startingTimeCompareTo(meetingTwo) < 0) {
							
							String check = mergedMeeting.startingHour + ":" + mergedMeeting.startingMinute + "|" + mergedMeeting.endHours + ":" + mergedMeeting.endMinutes;
							
							if(done.contains(check)) {
								i++;
								mergedMeeting = meetingCalendar1.get(i);
							} else {
								resultCalendar.add(mergedMeeting);
								mergedMeeting = meetingCalendar1.get(i);
								done.add(check);
							}
						}
						
					}
				}
			}
		}
		
		String check = mergedMeeting.startingHour + ":" + mergedMeeting.startingMinute + "|" + mergedMeeting.endHours + ":" + mergedMeeting.endMinutes;
		
		if(!done.contains(check)) {
			resultCalendar.add(mergedMeeting);
		}
		
		return resultCalendar;
		
	}
	
	private static final Meeting mergeDailyBounds(
			Meeting dailyBounds1,
			Meeting dailyBounds2) {
		
		int startingHour, startingMinute;
		int endHour, endMinute;
		
		if(dailyBounds1.startingTimeCompareTo(dailyBounds2) > 0) {
			startingHour = dailyBounds1.startingHour;
			startingMinute = dailyBounds1.startingMinute;
		} else if(dailyBounds2.startingTimeCompareTo(dailyBounds1) > 0) {
			startingHour = dailyBounds2.startingHour;
			startingMinute = dailyBounds2.startingMinute;
		} else {
			startingHour = dailyBounds1.startingHour;
			startingMinute = dailyBounds1.startingMinute;
		}
		
		if(dailyBounds1.endingTimeCompareTo(dailyBounds2) > 0) {
			endHour = dailyBounds2.endHours;
			endMinute = dailyBounds2.endMinutes;
		} else if(dailyBounds2.endingTimeCompareTo(dailyBounds1) > 0) {
			endHour = dailyBounds1.endHours;
			endMinute = dailyBounds1.endMinutes;
		} else {
			endHour = dailyBounds2.endHours;
			endMinute = dailyBounds2.endMinutes;
		}
		
		return new Meeting(startingHour, startingMinute, endHour, endMinute);
		
	}
	
	private static final Meeting mergeMeeting(
			Meeting meetingOne,
			Meeting meetingTwo) {
		
		int startingHour, startingMinute;
		int endHour, endMinute;
		
		if(meetingOne.startingTimeCompareTo(meetingTwo) > 0) {
			startingHour = meetingTwo.startingHour;
			startingMinute = meetingTwo.startingMinute;
		} else if(meetingTwo.startingTimeCompareTo(meetingOne) > 0) {
			startingHour = meetingOne.startingHour;
			startingMinute = meetingOne.startingMinute;
		} else {
			startingHour = meetingTwo.startingHour;
			startingMinute = meetingTwo.startingMinute;
		}
		
		if(meetingOne.endingTimeCompareTo(meetingTwo) > 0) {
			endHour = meetingOne.endHours;
			endMinute = meetingOne.endMinutes;
		} else if(meetingTwo.endingTimeCompareTo(meetingOne) > 0) {
			endHour = meetingTwo.endHours;
			endMinute = meetingTwo.endMinutes;
		} else {
			endHour = meetingOne.endHours;
			endMinute = meetingOne.endMinutes;
		}
		
		return new Meeting(startingHour, startingMinute, endHour, endMinute);
	}
	
	private static final Time calculatePotentialEndTime(
			Time currentTime,
			int meetingDuration) {
		
		int hours = currentTime.hour;
		int minutes = currentTime.minute;
		int meetingDurationHours, meetingDurationMinutes;
		
		if(meetingDuration > MINUTES_IN_AN_HOUR) {
			meetingDurationHours = getAdditionalHours(meetingDuration);
			int hourMinutes = meetingDurationHours * MINUTES_IN_AN_HOUR;
			meetingDurationMinutes = meetingDuration - hourMinutes;
		} else {
			meetingDurationHours = 0;
			meetingDurationMinutes = meetingDuration;
		}
		
		hours = hours + meetingDurationHours;
		minutes = minutes + meetingDurationMinutes;
		
		if(minutes > MINUTES_IN_AN_HOUR) {
			hours = hours + 1;
			minutes = minutes - MINUTES_IN_AN_HOUR;
		}
		
		return new Time(hours, minutes);
	}
	
	private static boolean isEndTimeWithinBounds(
			Meeting dailyBound, Time potentialEndTime) {
		if(potentialEndTime.hour >= dailyBound.startingHour  && potentialEndTime.hour < dailyBound.endHours) {
			return true;
		} else if(potentialEndTime.hour == dailyBound.startingHour) {
			if(potentialEndTime.minute >= dailyBound.startingMinute) {
				return true;
			} else {
				return false;
			}
		} else if(potentialEndTime.hour == dailyBound.endHours){
			if(potentialEndTime.minute <= dailyBound.endMinutes) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	private static boolean isMeetingWithinBounds(
			Meeting dailyBound,
			Meeting meeting) {
		
		if(dailyBound.startingTimeCompareTo(meeting) > 0) {
			return false;
		}
		
		if(dailyBound.endingTimeCompareTo(meeting) < 0) {
			return false;
		}
		
		return true;
		
	}
	
	private static final Meeting parseStringMeeting(StringMeeting meeting) {
		int startingHour, startingMinute, endHours, endMinutes;
		
		if(meeting.start.length() == 5) {
			startingHour =  Integer.parseInt(meeting.start.substring(0, 2));
			startingMinute = Integer.parseInt(meeting.start.substring(3));
		} else {
			startingHour =  Integer.parseInt(meeting.start.substring(0, 1));
			startingMinute = Integer.parseInt(meeting.start.substring(2));
		}
		
		if(meeting.end.length() == 5) {
			endHours = Integer.parseInt(meeting.end.substring(0, 2));
			endMinutes = Integer.parseInt(meeting.end.substring(3));
		} else {
			endHours = Integer.parseInt(meeting.end.substring(0, 1));
			endMinutes = Integer.parseInt(meeting.end.substring(2));
		}
		
		return new Meeting(startingHour, startingMinute, endHours, endMinutes);
	}
	
	private static final List<Meeting> parseStringMeetingCalendars(List<StringMeeting> stringMeetings){
		List<Meeting> meetings = new ArrayList<>();
		
		for(int i = 0; i < stringMeetings.size(); i++) {
			meetings.add(parseStringMeeting(stringMeetings.get(i)));
		}
		
		return meetings;
	}
	
	private static StringMeeting convertMeetingtoStringMeeting(Meeting meeting) {
		
		String start, end;
		
		start = meeting.startingHour + ":" + meeting.startingMinute;
		
		if(meeting.startingMinute >= 10) {
			start = meeting.startingHour + ":" + meeting.startingMinute;
		} else {
			start = meeting.startingHour + ":0" + meeting.startingMinute;
		}
		
		if(meeting.endMinutes >= 10) {
			end = meeting.endHours + ":" + meeting.endMinutes;
		} else {
			end = meeting.endHours + ":0" + meeting.endMinutes;
		}
		
		return new StringMeeting(start, end);
	}
	
	private static List<StringMeeting> convertMeetingsToStringMeetings(List<Meeting> messages){
		List<StringMeeting> stringMeetings = new ArrayList<>();
		
		for(int i = 0; i < messages.size(); i++) {
			stringMeetings.add(convertMeetingtoStringMeeting(messages.get(i)));
		}
		
		return stringMeetings;
	}
	
	private static int getAdditionalHours(int minutes) {
		return minutes/MINUTES_IN_AN_HOUR;
	}
	
	static class Time{
		int hour, minute;
		
		public Time(
				int hour,
				int minute) {
			this.hour = hour;
			this.minute = minute;
		}
		
		public Time(Time time) {
			this.hour = time.hour;
			this.minute = time.minute;
		}
		
		public String toString() {
			StringBuilder builder = new StringBuilder();
			
			builder.append(this.hour + ":");
			
			if(this.minute < 10) {
				builder.append("0" + this.minute);
			} else {
				builder.append(this.minute);
			}
			
			return builder.toString();
		}
	}
	
	static class Meeting implements Comparable<Meeting>{
		int startingHour, startingMinute, endHours, endMinutes;
		
		public Meeting(Meeting meeting) {
			this.startingHour = meeting.startingHour;
			this.startingMinute = meeting.startingMinute;
			this.endHours = meeting.endHours;
			this.endMinutes = meeting.endMinutes;
		}
		
		public Meeting(
				int startingHour,
				int startingMinute,
				int endHours,
				int endMinutes) {
			this.startingHour = startingHour;
			this.startingMinute = startingMinute;
			this.endHours = endHours;
			this.endMinutes = endMinutes;
		}
		
		public int startingTimeCompareTo(Object arg0) {
			if(arg0 instanceof Meeting) {
				
				Meeting meeting0 = (Meeting)arg0;
				
				if(this.startingHour < meeting0.startingHour) {
					return -1;
				} else if(this.startingHour > meeting0.startingHour) {
					return 1;
				} else {
					
					if(this.startingMinute < meeting0.startingMinute) {
						return -1;
					} else if(this.startingMinute > meeting0.startingMinute) {
						return 1;
					} else {
						return 0;
					}
					
				}
				
			} else {
				return -1;
			}
		}
		
		public int endingTimeCompareTo(Object arg0) {
			if(arg0 instanceof Meeting) {
				Meeting meeting0 = (Meeting)arg0;
				
				if(this.endHours < meeting0.endHours) {
					return -1;
				} else if(this.endHours > meeting0.endHours) {
					return 1;
				} else {
					
					if(this.endMinutes < meeting0.endMinutes) {
						return -1;
					} else if(this.endMinutes > meeting0.endMinutes) {
						return 1;
					} else {
						return 0;
					}
					
				}
			} else {
				return -1;
			}
		}
		
		public boolean isTimeStart(Time time) {
			
			if(time.hour > this.startingHour && time.hour < this.endHours) {
				return true;
			} else if(time.hour == this.startingHour) {
				if(time.minute >= this.startingMinute && (time.hour < this.endHours || (time.hour == this.endHours && time.minute < this.endMinutes))) {
					return true;
				} else {
					return false;
				}
			}
			
			return false;
		}
		
		public boolean isTimeIn(Time time) {
			
			if(time.hour > this.startingHour && time.hour < this.endHours) {
				return true;
			} else if(time.hour == this.startingHour) {
				if(time.minute > this.startingMinute) {
					return true;
				} else {
					return false;
				}
			} else if(time.hour == this.endHours) {
				
				if(time.minute < this.endMinutes) {
					return true;
				} else {
					return false;
				}
				
			} else {
				return false;
			}
		}
		
		public boolean overlapsMeeting(Meeting meeting) {
			
			if(this.startingHour < meeting.startingHour) {
				
				if(this.endHours > meeting.startingHour
						|| this.endHours > meeting.endHours) {
					return true;
				}
				
				if(this.endHours == meeting.startingHour) {
					
					if(this.endMinutes <= meeting.startingMinute) {
						return false;
					}
				}
			}
			
			if(this.startingHour == meeting.startingHour) {
				
				if(this.endHours == meeting.endHours) {
					
					if(this.startingHour < this.endHours) {
						return true;
					}
					
					if(this.startingHour == this.endHours) {
						
						if(this.startingMinute <= meeting.startingMinute) {
							return false;
						} else {
							return true;
						}
						
					}
					
				}
				
				if(this.endHours == this.startingHour) {
					if(this.endMinutes <= meeting.startingMinute) {
						return false;
					} else {
						return true;
					}
				}
				
				if(meeting.endHours == meeting.startingHour) {
					if(meeting.endMinutes <= this.startingMinute) {
						return false;
					} else {
						return true;
					}
				}
				
				return true;
				
			}
			
			if(this.startingHour > meeting.startingHour) {
				
				if(this.startingHour == meeting.endHours) {
					
					if(this.startingMinute >= meeting.endMinutes) {
						return false;
					} else {
						return true;
					}
				}
				
				if(this.startingHour < meeting.endHours) {
					return true;
				}
				
				if(this.startingHour > meeting.endHours) {
					return false;
				}
				
			}
			
			return false;
			
		}

		@Override
		public int compareTo(Meeting arg0) {
			
			if(this.startingTimeCompareTo(arg0) > 0) {
				return 1;
			} else if(this.startingTimeCompareTo(arg0) < 0) {
				return -1;
			} else {
				
				if(this.endingTimeCompareTo(arg0) > 0) {
					return 1;
				} else if(this.endingTimeCompareTo(arg0) < 0) {
					return -1;
				} else {
					return 0;
				}
				
			}
			
		}
	}
	
	static class StringMeeting {
	    public String start;
	    public String end;

	    public StringMeeting(String start, String end) {
	      this.start = start;
	      this.end = end;
	    }
	  }
}
